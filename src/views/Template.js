import React, { Component } from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min';
import { Container } from 'react-bootstrap';
import { Button } from 'react-bootstrap';

import {
  Route,
  NavLink,
  BrowserRouter
} from "react-router-dom";

import SplineChart from "./line charts/Spline Chart";

class Template extends Component {
  
  render() {    
    return (
		<div>				  
      			<Container className="mb-2">
					<Button variant="success"
						onClick={this.forceUpdate}>Refresh</Button>
				</Container>
								<SplineChart/>
			</div>
    );
  }
}

export default Template;