import React, { Component } from 'react';
import CanvasJSReact from '../../assets/canvasjs.react';
import axios from 'axios';
import dayjs from 'dayjs';

import { Container } from 'react-bootstrap';
import { Button } from 'react-bootstrap';


var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class SplineChart extends Component {
	constructor(props) {
		super(props);
		this.state = {
			points: [],
			stripline: null
		};

		setInterval(() => {
			this.readData();
		}, 1000 * 60 * 5);

		this.readData();
	}


	readData() {
		const searchParams = new URLSearchParams();
		searchParams.append("start", dayjs().subtract('4', 'hours').toISOString());
		searchParams.append("end", dayjs().add('24', 'hours').toISOString());
		console.log(searchParams.toString());

		

		

		axios.get('https://dashboard.elering.ee/api/nps/price?' + searchParams.toString())
			.then(res => {
	
				const data = res.data;
				this.setState({
					points: data.data.ee.map(function (val, index) {
						return {
							x: dayjs(val.timestamp * 1000).toDate(),
							y: val.price
						};
					}),
					stripline:
					{
						startValue: dayjs().startOf('hour'),
						endValue: dayjs().endOf('hour'),
						color: "#d8d8d8",
						labelFontColor: "#a8a8a8",
						labe: "Current hour"
					}
				});

				this.forceUpdate();
			})


		console.log(this.state.points);
	}

	render() {
		const options = {
			animationEnabled: true,
			title: {
				text: "Electricity cost"
			},
			axisX: {
				valueFormatString: "DD.MM HH:00",
				stripLines: [
					this.state.stripline
				]
			},
			axisY: {
				title: "Price",
				prefix: "EUR",
				includeZero: true
			},
			data: [{
				yValueFormatString: "##.##",
				xValueFormatString: "DD.MM HH:00",
				type: "spline",
				dataPoints: this.state.points,
			}]
		}

		return (
			<Container>
	
				<Container>
					<CanvasJSChart options={options}
					/* onRef={ref => this.chart = ref} */
					/>
					{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
				</Container>
			</Container>
		);
	}
}

export default SplineChart;                           